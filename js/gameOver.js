var gameOverState = {
    preload: function() {
        var playerName = game.global.playerName;
        var recordsRef = firebase.database().ref('record_list');
        var record ={
            playerName: playerName,
            score: game.global.score,
        };
        recordsRef.push(record);
    },
    create: function() {
        for(var i = 0; i<12; i++) {
            if(i < totalRecord.length) {
                var rankLabel = game.add.text(game.width/2 - 200, 180 + 40*i, i+1 + '.',
                    { font: '35px Arial', fill: '#ffffff' });
                rankLabel.anchor.setTo(1.0, 0.5);
                var recordLabel = game.add.text(game.width/2 - 150, 180 + 40*i, totalRecord[i].name,
                    { font: '35px Arial', fill: '#ffffff' });
                recordLabel.anchor.setTo(0.0, 0.5);
                var scoreLabel = game.add.text(game.width/2 + 200, 180 + 40*i, totalRecord[i].score,
                    { font: '35px Arial', fill: '#ffffff' });
                scoreLabel.anchor.setTo(0.0, 0.5);
            } else {
                var recordLabel = game.add.text(game.width/2, 180 + 40*i, i+1 + '.        --------' ,
                    { font: '35px Arial', fill: '#ffffff' });
                recordLabel.anchor.setTo(0.0, 0.5);
            }
        }
        if(!game.global.winGame) {
            var gameOverLabel = game.add.text(game.width/2, 60, 'Game Over', { font: '35px Arial', fill: '#ff0000' });
            gameOverLabel.anchor.setTo(0.5, 0.5);
        } else {
            var gameWinLabel = game.add.text(game.width/2, 60, 'Game Win', { font: '35px Arial', fill: '#0000ff' });
            gameWinLabel.anchor.setTo(0.5, 0.5);
        }
        var startLabel = game.add.text(game.width/2, game.height-70, 'press the up arrow key to restart', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        var quitLabel = game.add.text(game.width/2, game.height-30, 'press the down arrow key to go back to menu', { font: '25px Arial', fill: '#ffffff' });
        quitLabel.anchor.setTo(0.5, 0.5);
        this.highestScoreLabel = game.add.text(game.width/2, 100, 'Your Highest Score: ' + game.global.highestScore, { font: '25px Arial', fill: '#ffffff' });
        this.highestScoreLabel.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(game.width/2, 140, 'Your Score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        upKey.onDown.add(this.start, this);
        downKey.onDown.add(this.quit, this);
    },
    start: function() {
        if(game.global.gameMode == 2) game.state.start('twoPlayerMain');
        else game.state.start('main');
    },
    quit: function() {
        game.state.start('menu');
    }
}; 