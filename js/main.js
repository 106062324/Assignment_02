var recordsRef = firebase.database().ref('record_list');
var flag = 0;
var record_first_count = -1;
var record_second_count = 0;
var totalRecord = [];
recordsRef.once('value')
    .then(function(snapshot) {
        recordsRef.on('value', function(snapshot) {
            record_second_count = 0;
            for(let i in snapshot.val()) {
                if(record_first_count < record_second_count) {
                    record_first_count++;
                    totalRecord.push({name: snapshot.val()[i].playerName, score: snapshot.val()[i].score});
                }
                record_second_count++;
            }
            totalRecord = totalRecord.sort(function (a, b) {
                return a.score < b.score ? 1 : -1;
            });
        })
    })
// Initialize Phaser
var game = new Phaser.Game(1280, 720, Phaser.AUTO, document.getElementById('game'));
// Define our global variable
game.global = {
    score: 0,
    highestScore: 0,
    level: 0,
    playerName: 'Frank',
    winGame: false,
    gameMode: 1,
}
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('gameOver', gameOverState);
game.state.add('main', mainState);
game.state.add('twoPlayerMain', TwoPlayerState);
// Start the 'boot' state
game.state.start('boot');