var TwoPlayerState = {
    preload: function() {},
    create: function() {
        game.stage.backgroundColor = '#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
        // keyboard
        this.cursor = game.input.keyboard.createCursorKeys();
        this.key1 = this.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.key2 = this.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.keyA = this.input.keyboard.addKey(Phaser.Keyboard.A);
        this.keyC = this.input.keyboard.addKey(Phaser.Keyboard.C);
        this.keyD = this.input.keyboard.addKey(Phaser.Keyboard.D);
        this.keyJ = this.input.keyboard.addKey(Phaser.Keyboard.J);
        this.keyK = this.input.keyboard.addKey(Phaser.Keyboard.K);
        this.keyL = this.input.keyboard.addKey(Phaser.Keyboard.L);
        this.keyP = this.input.keyboard.addKey(Phaser.Keyboard.P);
        this.keyS = this.input.keyboard.addKey(Phaser.Keyboard.S);
        this.keyV = this.input.keyboard.addKey(Phaser.Keyboard.V);
        this.keyW = this.input.keyboard.addKey(Phaser.Keyboard.W);
        this.keyX = this.input.keyboard.addKey(Phaser.Keyboard.X);
        this.keyZ = this.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.keySpace = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        //audio
        this.bgMusic = game.add.audio('bgMusic');
        this.bgMusic.play();
        // background
        this.milkyWay = game.add.sprite(0, 0, 'milkyWay');
        this.milkyWay.smoothed = false;
        this.milkyWay.scale.setTo(0.8, 0.8);
        this.milkyWay.alpha = 0.2;
        this.layer0 = game.add.sprite(0, 0, 'background0');
        this.layer0.smoothed = false;
        this.layer0.scale.setTo(5.0, 5.0);
        this.layer1 = game.add.sprite(0, game.height, 'background0');
        this.layer1.smoothed = false;
        this.layer1.scale.setTo(5.0, 5.0);
        this.layer1.y = (this.layer1.height * -1);
        this.layer1.y += 1000;
        game.physics.arcade.enable(this.layer1);
        this.layer1.enableBody = true;
        this.layer1.body.velocity.y = 100;
        this.layer2 = game.add.sprite(0, game.height, 'background0');
        this.layer2.smoothed = false;
        this.layer2.scale.setTo(5.0, 5.0);
        this.layer2.y = (this.layer2.height * -1);
        game.physics.arcade.enable(this.layer2);
        this.layer2.enableBody = true;
        this.layer2.body.velocity.y = 0;
        // player1
        this.player1 = game.add.sprite(game.width/2 + 120, game.height/2 + 180, 'player');
        this.player1.animations.add('moving', [0, 3], 8, true);
        this.player1.smoothed = false;
        this.player1.scale.setTo(3.0, 3.0);
        this.player1.anchor.setTo(0.5, 0.0);
        game.physics.arcade.enable(this.player1);
        this.player1.body.collideWorldBounds = true;
        this.player1.moveUp = false;
        this.player1.moveDown = false;
        this.player1.moveLeft = false;
        this.player1.moveRight = false;
        this.player1.maxHealth = 100;
        this.player1.health = 100;
        this.player1.skillPower = 0;
        // player2
        this.player2 = game.add.sprite(game.width/2 - 120, game.height/2 + 180, 'player2');
        this.player2.animations.add('moving', [0, 3], 8, true);
        this.player2.smoothed = false;
        this.player2.scale.setTo(3.0, 3.0);
        this.player2.anchor.setTo(0.5, 0.0);
        game.physics.arcade.enable(this.player2);
        this.player2.body.collideWorldBounds = true;
        this.player2.moveUp = false;
        this.player2.moveDown = false;
        this.player2.moveLeft = false;
        this.player2.moveRight = false;
        this.player2.maxHealth = 100;
        this.player2.health = 100;
        this.player2.skillPower = 0;
        // player laser
        this.player1.playerShooting = false;
        this.player1.spreadLaserOn = false;
        this.player1.missileOn = false;
        this.player2.playerShooting = false;
        this.player2.spreadLaserOn = false;
        this.player2.missileOn = false;
        this.playerLasers = this.add.group();
        this.playerLasers.enableBody = true;
        this.playerLasers.createMultiple(160, 'playerLaser');
        game.time.events.loop(100, function() {this.addPlayerLaser(this.player1)}, this);
        game.time.events.loop(100, function() {this.addPlayerLaser(this.player2)}, this);
        game.time.events.loop(100, function() {this.addSpreadLaser(this.player1)}, this);
        game.time.events.loop(100, function() {this.addSpreadLaser(this.player2)}, this);
        // missile
        this.playerMissiles = this.add.group();
        this.playerMissiles.enableBody = true;
        this.playerMissiles.createMultiple(12, 'missile');
        game.time.events.loop(100, function() {this.addPlayerMissile(this.player1)}, this);
        game.time.events.loop(100, function() {this.addPlayerMissile(this.player2)}, this);
        this.playerMissiles.forEach(function(missile){
            missile.animations.add('flying', [0, 3], 8, true);
            missile.turnRate = 5;
            missile.shipTrail = game.add.emitter(0, game.height, 40);
            missile.shipTrail.width = 10;
            missile.shipTrail.makeParticles('plasma');
            missile.shipTrail.setXSpeed(30, -30);
            missile.shipTrail.setYSpeed(200, 180);
            missile.shipTrail.setRotation(50,-50);
            missile.shipTrail.setAlpha(1, 0.01, 800);
            missile.shipTrail.setScale(2, 0, 2, 0, 2000, Phaser.Easing.Linear.InOut);
            missile.shipTrail.start(false, 500, 10);
        }, this);
        // player emitter
        this.player1.emitter = game.add.emitter(0, 0, 50);
        this.player1.emitter.makeParticles('playerLaser');
        this.player1.emitter.setScale(2, 0, 2, 0, 1000);
        this.player1.emitter.gravity = 0;
        this.player1.emitter1 = game.add.emitter(0, 0, 50);
        this.player1.emitter1.makeParticles('pixel');
        this.player1.emitter1.setScale(2, 0, 2, 0, 800);
        this.player1.emitter1.gravity = 0;
        this.player2.emitter = game.add.emitter(0, 0, 50);
        this.player2.emitter.makeParticles('playerLaser');
        this.player2.emitter.setScale(2, 0, 2, 0, 1000);
        this.player2.emitter.gravity = 0;
        this.player2.emitter1 = game.add.emitter(0, 0, 50);
        this.player2.emitter1.makeParticles('pixel');
        this.player2.emitter1.setScale(2, 0, 2, 0, 800);
        this.player2.emitter1.gravity = 0;
        // enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(15, 'enemy');
        game.time.events.loop(1000, this.addEnemy, this);
        // enemy laser
        this.enemyLasers = this.add.group();
        this.enemyLasers.enableBody = true;
        this.enemyLasers.createMultiple(20, 'pixel');
        // enemy emitter
        this.enemies.forEach(function(enemy) {
            enemy.emitter = game.add.emitter(0, 0, 50);
            enemy.emitter.makeParticles('pixel');
            enemy.emitter.setScale(2, 0, 2, 0, 1000);
            enemy.emitter.gravity = 0;
            enemy.animations.add('enemyMoving', [0, 3], 8, true);
            enemy.explosion = game.add.audio('explosion');
        }, this);
        //power UP
        this.powerUps = game.add.group();
        this.powerUps.enableBody = true;
        this.powerUps.createMultiple(15, 'powerUp');
        game.time.events.loop(3000, this.addPowerUp, this);
        // health bar
        this.healthLabel1 = game.add.text(60, 50, 'P1  HP: ' + this.player1.health, { font: '25px Arial', fill: '#ffffff' });
        this.healthLabel1.anchor.setTo(0.0, 0.5);
        this.healthLabel2 = game.add.text(game.width - 300, 50, 'P2 HP: ' + this.player2.health, { font: '25px Arial', fill: '#ffffff' });
        this.healthLabel2.anchor.setTo(0.0, 0.5);
        // score
        game.global.score = 0;
        this.scoreLabel = game.add.text(game.width/2, 70, 'Score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        // skill power
        this.powerLabel1 = game.add.text(200, 50, 'Power: ' + this.player1.skillPower, { font: '25px Arial', fill: '#ffffff' });
        this.powerLabel1.anchor.setTo(0.0, 0.5);
        this.powerLabel2 = game.add.text(game.width - 160, 50, 'Power: ' + this.player2.skillPower, { font: '25px Arial', fill: '#ffffff' });
        this.powerLabel2.anchor.setTo(0.0, 0.5);
        // level
        game.global.level = 1;
        this.levelLabel = game.add.text(game.width/2, 40, 'Level: ' + game.global.level, { font: '25px Arial', fill: '#ffffff' });
        this.levelLabel.anchor.setTo(0.5, 0.5);
        game.global.winGame = false;
        game.global.gameMode = 2;
    },
    update: function() {
        //this.player1.rotation = Math.PI/2;
        this.playerMoving();
        this.movePlayer(this.player1);
        this.movePlayer(this.player2);
        this.playerShoot();
        this.changeVolume();
        this.checkGameWin();
        this.powerUps.forEachAlive(this.powerUpUpdate, this);
        this.playerMissiles.forEach(this.missileUpdate, this);
        game.physics.arcade.overlap(this.player1, this.enemies, this.enemyHitPlayer, null, this);
        game.physics.arcade.overlap(this.player1, this.enemies, this.enemyHitPlayer, null, this);
        game.physics.arcade.overlap(this.player1, this.enemyLasers, this.laserHitPlayer, null, this);
        game.physics.arcade.overlap(this.player1, this.powerUps, this.getPowerUp, null, this);
        game.physics.arcade.overlap(this.player2, this.enemies, this.enemyHitPlayer, null, this);
        game.physics.arcade.overlap(this.player2, this.enemies, this.enemyHitPlayer, null, this);
        game.physics.arcade.overlap(this.player2, this.enemyLasers, this.laserHitPlayer, null, this);
        game.physics.arcade.overlap(this.player2, this.powerUps, this.getPowerUp, null, this);
        game.physics.arcade.overlap(this.playerMissiles, this.enemies, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.playerLasers, this.enemies, this.hitEnemy, null, this);
        game.world.bringToTop(this.enemies);
        this.healthLabel1.text = 'P1  HP: ' + this.player1.health;
        this.healthLabel2.text = 'P2 HP: ' + this.player2.health;
        this.powerLabel1.text = 'Power: ' + this.player1.skillPower;
        this.powerLabel2.text = 'Power: ' + this.player2.skillPower;
        this.scoreLabel.text = 'score: ' + game.global.score;
        game.world.bringToTop(this.healthLabel1);
        game.world.bringToTop(this.healthLabel2);
        game.world.bringToTop(this.scoreLabel);
        if(game.global.score >= game.global.highestScore) game.global.highestScore = game.global.score;
        this.backgroundUpdate();
        game.input.onDown.add(function() {
            if(game.paused) {
               if(this.keyP.isDown) game.paused = false;
            }
        }, this);
        if(game.paused) {
            if(this.keyP.isDown) game.paused = false;
        } else {
            if(this.keyP.isDown) game.paused = true;
        }
    },
   movePlayer: function(player) {
        player.animations.play('moving');
        if(player.moveLeft) {
            player.body.velocity.x = -500;
        }
        else if(player.moveRight) {
            player.body.velocity.x = 500;
        } else {
            if(player.body.velocity.x != 0) {
                if(player.body.velocity.x > 0) {
                    if(player.body.velocity.x > 200) player.body.velocity.x -= 20;
                    else player.body.velocity.x -= 5;
                } else if(player.body.velocity.x < 0) {
                    if(player.body.velocity.x < -200) player.body.velocity.x += 20;
                    else player.body.velocity.x += 5;
                }
            }
        }
        if(player.moveUp) {
            player.body.velocity.y = -500;
        }
        else if(player.moveDown) {
            player.body.velocity.y = 500;
        } else {
            if(player.body.velocity.y != 0) {
                if(player.body.velocity.y > 0) {
                    if(player.body.velocity.y > 200) player.body.velocity.y -= 20;
                    else player.body.velocity.y -= 5;
                } else if(player.body.velocity.y < 0) {
                    if(player.body.velocity.y < -200) player.body.velocity.y += 20;
                    else player.body.velocity.y += 5;
                }
            }
        }
        if(this.keyV.isDown) {
            if(player.body.velocity.x != 0) {
                if(player.body.velocity.x > 0) {
                    player.body.velocity.x -= 20;
                } else if(player.body.velocity.x < 0) {
                    player.body.velocity.x += 20;
                }
            }
            if(player.body.velocity.y != 0) {
                if(player.body.velocity.y > 0) {
                    player.body.velocity.y -= 20;
                } else if(player.body.velocity.y < 0) {
                    player.body.velocity.y += 20;
                }
            }
        }
    },
    playerShoot: function() {
        if(this.keyZ.isDown) this.player1.playerShooting = true;
        else this.player1.playerShooting = false;
        if(this.keyX.isDown) this.player1.missileOn = true;
        else this.player1.missileOn = false;
        if(this.keyC.isDown) this.player1.spreadLaserOn = true;
        else this.player1.spreadLaserOn = false;
        // player 2
        if(this.keyJ.isDown) this.player2.playerShooting = true;
        else this.player2.playerShooting = false;
        if(this.keyK.isDown) this.player2.missileOn = true;
        else this.player2.missileOn = false;
        if(this.keyL.isDown) this.player2.spreadLaserOn = true;
        else this.player2.spreadLaserOn = false;
    },
    playerMoving: function() {
        if(this.cursor.up.isDown) this.player1.moveUp = true;
        else this.player1.moveUp = false;
        if(this.cursor.down.isDown) this.player1.moveDown = true;
        else this.player1.moveDown = false;
        if(this.cursor.left.isDown) this.player1.moveLeft = true;
        else this.player1.moveLeft = false;
        if(this.cursor.right.isDown) this.player1.moveRight = true;
        else this.player1.moveRight = false;
        if(this.keyW.isDown) this.player2.moveUp = true;
        else this.player2.moveUp = false;
        if(this.keyS.isDown) this.player2.moveDown = true;
        else this.player2.moveDown = false;
        if(this.keyA.isDown) this.player2.moveLeft = true;
        else this.player2.moveLeft = false;
        if(this.keyD.isDown) this.player2.moveRight = true;
        else this.player2.moveRight = false;
    },
    addEnemy: function() {
        if(game.global.level >= 5) return;
        for(var i = 0; i<game.global.level; i++) {
            var enemy = this.enemies.getFirstDead();
            if (!enemy) return;
            enemy.smoothed = false;
            enemy.scale.setTo(3.0, 3.0);
            enemy.anchor.setTo(0.5, 0.5);
            enemy.reset(game.rnd.integerInRange(100, game.width - 100), 0);
            enemy.body.velocity.x = game.rnd.integerInRange(-50, 50);
            enemy.body.velocity.y = game.rnd.integerInRange(100, 300);
            enemy.checkWorldBounds = true;
            enemy.outOfBoundsKill = true;
            enemy.animations.play('enemyMoving');
            // enemy laser
            game.time.events.loop(1000, function() {this.addEnemyLaser(enemy);}, this);
        }
    },
    addPlayerLaser: function(player) {
        var playerLaser = this.playerLasers.getFirstDead();
        if(!playerLaser || !player.playerShooting || !player.alive) return;
        playerLaser.smoothed = false;
        playerLaser.anchor.setTo(0.5, 1);
        playerLaser.scale.y *= -1;
        playerLaser.scale.setTo(3.0, 3.0);
        playerLaser.reset(player.x, player.y);
        playerLaser.angle = 0;
        playerLaser.body.velocity.y = -600;
        playerLaser.checkWorldBounds = true;
        playerLaser.outOfBoundsKill = true;
    },
    addEnemyLaser: function(enemy) {
        var enemyLaser = this.enemyLasers.getFirstDead();
        if(!enemyLaser || !enemy.alive) return;
        enemyLaser.smoothed = false;
        enemyLaser.anchor.setTo(0.5, 1);
        enemyLaser.scale.y *= -1;
        enemyLaser.scale.setTo(3.0, 3.0);
        enemyLaser.reset(enemy.x, enemy.y);
        enemyLaser.body.velocity.y = 500;
        enemyLaser.checkWorldBounds = true;
        enemyLaser.outOfBoundsKill = true;
    },
    addPowerUp: function() {
        var powerUp = this.powerUps.getFirstDead();
        if (!powerUp) return;
        powerUp.smoothed = false;
        powerUp.scale.setTo(1.75, 1.75);
        powerUp.anchor.setTo(0.5, 0.5);
        powerUp.reset(game.rnd.integerInRange(100, game.width - 100), 0);
        powerUp.body.velocity.y = game.rnd.integerInRange(100, 150);
        powerUp.checkWorldBounds = true;
        powerUp.outOfBoundsKill = true;
    },
    enemyHitPlayer: function(player, enemy) {
        if(enemy) {
            // emitter
            enemy.emitter.x = enemy.x;
            enemy.emitter.y = enemy.y;
            enemy.emitter.start(true, 800, null, 15);
            // kill
            player.health -= 10;
            if(player.health <= 0) {
                player.health = 0;
                player.emitter.x = player.x;
                player.emitter.y = player.y;
                player.emitter.start(true, 800, null, 15);
                player.kill();
                game.camera.shake(0.02, 300);
                if(!this.player1.alive && !this.player2.alive) {
                    game.time.events.add(1000, function() {game.state.start('gameOver');}, this);
                }
            }
            else {
                game.camera.shake(0.004, 300);
            }
            enemy.kill();
        }
    },
    laserHitPlayer: function(player, enemyLaser) {
        if(enemyLaser) {
            // emitter
            player.emitter1.x = player.x;
            player.emitter1.y = player.y;
            player.emitter1.start(true, 800, null, 15);
            // kill
            player.health -= 5;
            if(player.health <= 0) {
                player.health = 0;
                player.emitter.x = player.x;
                player.emitter.y = player.y;
                player.emitter.start(true, 800, null, 15);
                player.kill();
                game.camera.shake(0.02, 300);
                if(!this.player1.alive && !this.player2.alive) {
                    game.time.events.add(1000, function() {game.state.start('gameOver');}, this);
                }
            }
            else {
                game.camera.shake(0.004, 300);
            }
            enemyLaser.kill();
        }
    },
    hitEnemy: function(playerLaser, enemy) {
        if (enemy) {
            // score
            game.global.score += 5;
            // emitter
            enemy.emitter.x = enemy.x;
            enemy.emitter.y = enemy.y;
            enemy.emitter.start(true, 800, null, 15);
            // kill
            enemy.explosion.play();
            this.player1.skillPower += 5;
            this.player2.skillPower += 5;
            enemy.kill();
            playerLaser.kill();
            game.camera.shake(0.004, 300);
        }
    },
    powerUpUpdate: function(powerUp) {
        //power Up
        var velocity = powerUp.body.velocity.y;
        
        powerUp.angle += 0.5;
        powerUp.body.velocity.x = 0;
        powerUp.body.velocity.y = velocity;
    },
    backgroundUpdate: function() {
        if(this.layer1.y > -500) {
            this.layer2.y = -2000;
            this.layer2.body.velocity.y = 100;
            if(this.layer1.y > 100) {
                this.layer1.y = -2000;
                this.layer1.y.velocity = 0;
                game.global.level++;
                this.levelLabel.text = 'Level: ' + game.global.level;
            }
        } else if(this.layer2.y > -500) {
            this.layer1.body.velocity.y = 100;
            if(this.layer2.y > 100) {
                this.layer2.y = -2000;
                this.layer2.body.velocity.y = 0;
            }
        }
        this.milkyWay.x = -0.01*this.player1.x;
        this.milkyWay.y = -0.01*this.player1.y;
    },
}

TwoPlayerState.movePlayer2 = function() {
    this.player2.animations.play('moving');
        if(this.cursor.left.isDown) {
            this.player1.body.velocity.x = -500;
        }
        else if(this.cursor.right.isDown) {
            this.player1.body.velocity.x = 500;
        } else {
            if(this.player1.body.velocity.x != 0) {
                if(this.player1.body.velocity.x > 0) {
                    if(this.player1.body.velocity.x > 200) this.player1.body.velocity.x -= 20;
                    else this.player1.body.velocity.x -= 5;
                } else if(this.player1.body.velocity.x < 0) {
                    if(this.player1.body.velocity.x < -200) this.player1.body.velocity.x += 20;
                    else this.player1.body.velocity.x += 5;
                }
            }
        }
        if(this.cursor.up.isDown) {
            this.player1.body.velocity.y = -500;
        }
        else if(this.cursor.down.isDown) {
            this.player1.body.velocity.y = 500;
        } else {
            if(this.player1.body.velocity.y != 0) {
                if(this.player1.body.velocity.y > 0) {
                    if(this.player1.body.velocity.y > 200) this.player1.body.velocity.y -= 20;
                    else this.player1.body.velocity.y -= 5;
                } else if(this.player1.body.velocity.y < 0) {
                    if(this.player1.body.velocity.y < -200) this.player1.body.velocity.y += 20;
                    else this.player1.body.velocity.y += 5;
                }
            }
        }
        if(this.keyA.isDown) {
            if(this.player1.body.velocity.x != 0) {
                if(this.player1.body.velocity.x > 0) {
                    this.player1.body.velocity.x -= 20;
                } else if(this.player1.body.velocity.x < 0) {
                    this.player1.body.velocity.x += 20;
                }
            }
            if(this.player1.body.velocity.y != 0) {
                if(this.player1.body.velocity.y > 0) {
                    this.player1.body.velocity.y -= 20;
                } else if(this.player1.body.velocity.y < 0) {
                    this.player1.body.velocity.y += 20;
                }
            }
        }
};

TwoPlayerState.getPowerUp =  function(player, powerUp) {
    player.heal(10);
    powerUp.kill();
};

TwoPlayerState.addPlayerMissile = function(player) {
    var playerMissile = this.playerMissiles.getFirstDead();
    if(!playerMissile || !(player.skillPower >= 10) || !player.missileOn || !player.alive) return;
    player.skillPower -= 10;
    playerMissile.smoothed = false;
    playerMissile.anchor.setTo(0.5, 0.5);
    playerMissile.scale.setTo(0.6, 0.6);
    playerMissile.reset(player.x, player.y - 10);
    playerMissile.angle = 0;
    playerMissile.body.velocity.y = -800;
    playerMissile.animations.play('flying');
    playerMissile.checkWorldBounds = true;
    playerMissile.outOfBoundsKill = true;
};

TwoPlayerState.missileUpdate = function(missile) {
    var enemy = this.enemies.getFirstAlive();
    if (!missile.alive) {
        missile.shipTrail.on = false;
        return;
    } else {
        missile.shipTrail.on = true;
        missile.shipTrail.x = missile.x - Math.cos(missile.rotation - Math.PI/2) * 50;
        missile.shipTrail.y = missile.y - Math.sin(missile.rotation - Math.PI/2) * 60;
        missile.shipTrail.setXSpeed(1*(-missile.body.velocity.x), 0.5*(-missile.body.velocity.x));
        missile.shipTrail.setYSpeed(1*(-missile.body.velocity.y), 0.5*(-missile.body.velocity.y));
    }
    if(!enemy || !missile.alive) return;
    var targetAngle = game.math.angleBetween(missile.x, missile.y, enemy.x, enemy.y);
    targetAngle += Math.PI/2;
    if (missile.rotation !== targetAngle) {
        var delta = targetAngle - missile.rotation;

        if (delta > Math.PI) delta -= Math.PI * 2;
        if (delta < (-Math.PI)) delta += Math.PI * 2;
        if (delta > 0) {
            // Turn clockwise
            missile.angle += missile.turnRate;
        } else {
            // Turn counter-clockwise
            missile.angle -= missile.turnRate;
        }

        if (Math.abs(delta) < game.math.degToRad(missile.turnRate)) {
            missile.rotation = targetAngle;
        }
    }
    missile.body.velocity.x = Math.cos(missile.rotation - Math.PI/2) * 600;
    missile.body.velocity.y = Math.sin(missile.rotation - Math.PI/2) * 600;
};

TwoPlayerState.missileHitEnemy = function(missile, enemy) {
    if (enemy) {
        // score
        game.global.score += 5;
        // emitter
        enemy.emitter.x = enemy.x;
        enemy.emitter.y = enemy.y;
        enemy.emitter.start(true, 800, null, 15);
        // kill
        missile.shipTrail.on = false;
        enemy.kill();
        missile.kill();
        game.camera.shake(0.004, 300);
    }
};

TwoPlayerState.addSpreadLaser =  function(player) {
    if(!player.spreadLaserOn || !player.alive || player.skillPower < 1) return;
    player.skillPower -= 1;
    for(var i = 0; i<9; i++) {
        var playerLaser = this.playerLasers.getFirstDead();
        if(!playerLaser) return;
        playerLaser.smoothed = false;
        playerLaser.anchor.setTo(0.5, 1);
        playerLaser.scale.y *= -1;
        playerLaser.scale.setTo(3.0, 3.0);
        playerLaser.reset(player.x, player.y);
        playerLaser.angle = 5*(4- i);
        playerLaser.body.velocity.x = (i-4)*(-80);
        playerLaser.body.velocity.y = Math.sin(playerLaser.rotation + Math.PI/2)*(-800);
        playerLaser.checkWorldBounds = true;
        playerLaser.outOfBoundsKill = true;
    }
};

TwoPlayerState.changeVolume =  function() {
    if(this.key1.isDown) {
        this.enemies.forEach(function(enemy) {
            if(enemy.explosion.volume >= 0.1) enemy.explosion.volume -= 0.1;
            if(enemy.explosion.volume < 0) enemy.explosion.volume = 0;
        }, this);
        this.bgMusic.volume -= 0.1;
        if(this.bgMusic.volume < 0) {
            this.bgMusic.volume = 0;
        }
    } else if(this.key2.isDown) {
        this.enemies.forEach(function(enemy) {
            if(enemy.explosion.volume <= 0.9)enemy.explosion.volume += 0.1;
            if(enemy.explosion.volume > 1) enemy.explosion.volume = 1;
        }, this);
        if(this.bgMusic.volume <= 0.9) this.bgMusic.volume += 0.1;
        if(this.bgMusic.volume > 1) this.bgMusic.volume = 1;
    }
};

TwoPlayerState.checkGameWin = function() {
    if(game.global.level >= 5) {
        game.time.events.add(1000, function() {game.state.start('gameOver');}, this);
        game.global.winGame = true;
    }
};