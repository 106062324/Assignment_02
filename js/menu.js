var menuState = {
    create: function() {
        this.keyX = this.input.keyboard.addKey(Phaser.Keyboard.X);
        this.keyZ = this.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.milkyWay = game.add.sprite(0, 0, 'milkyWay');
        this.milkyWay.smoothed = false;
        this.milkyWay.scale.setTo(0.8, 0.8);
        this.milkyWay.alpha = 0.4;
        this.layer0 = game.add.sprite(0, 0, 'background0');
        this.layer0.smoothed = false;
        this.layer0.scale.setTo(5.0, 5.0);
        var nameLabel = game.add.text(game.width/2, 140, 'Raiden', { font: '80px monospace', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        this.btnPlay = this.add.sprite(game.width/2, game.height/2, "sprBtnPlayHover");
        this.btnPlay.anchor.setTo(0.5, 0.5);
        var startLabel1 = game.add.text(game.width/2, game.height-200,
        'press the z key to start 1P Game', { font: '25px Arial', fill: '#ffffff' });
        startLabel1.anchor.setTo(0.5, 0.5);
        var startLabel1 = game.add.text(game.width/2, game.height-140,
        'press the x key to start 2P Game', { font: '25px Arial', fill: '#ffffff' });
        startLabel1.anchor.setTo(0.5, 0.5);
        this.keyX.onDown.add(function() {game.state.start('twoPlayerMain');}, this);
        this.keyZ.onDown.add(function() {game.state.start('main');}, this);
        game.time.events.add(500, this.inputPlayerName, this);
        this.bgMusic = game.add.audio('bgMusic');
        this.bgMusic.play();
    },
    inputPlayerName: function() {
        var player = prompt("Please enter your name:", 'player');
        if (player == null || player == "") {
            game.global.playerName = 'player';
        } else {
            game.global.playerName = player;
        }
    },
}; 