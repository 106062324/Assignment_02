var loadState = {
    preload: function() {
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, game.height/2, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // main
        game.load.spritesheet('player', 'assets/Player.png', 16, 16);
        game.load.spritesheet('player2', 'assets/Player2.png', 16, 16);
        game.load.spritesheet('enemy', 'assets/Enemy.png', 16, 16);
        game.load.spritesheet('missile', 'assets/missile1.png', 33, 141);
        game.load.image('playerLaser', 'assets/playerLaser.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('plasma', 'assets/plasma.png');
        game.load.image('background0', 'assets/background0.png');
        game.load.image('milkyWay', 'assets/milkyWay.png');
        game.load.image('healthbar', 'assets/healthbar.png');
        game.load.image('powerUp', 'assets/powerUp.png');
        // menu
        game.load.image('sprBtnPlay', 'assets/menu/sprBtnPlay.png');
        game.load.image('sprBtnPlayDown', 'assets/menu/sprBtnPlayDown.png');
        game.load.image('sprBtnPlayHover', 'assets/menu/sprBtnPlayHover.png');
        // gameover
        game.load.image('sprBtnRestart', 'assets/menu/sprBtnRestart.png');
        game.load.image('sprBtnRestartDown', 'assets/menu/sprBtnRestartDown.png');
        game.load.image('sprBtnRestartHover', 'assets/menu/sprBtnRestartHover.png');
        // audio
        game.load.audio('explosion', 'assets/audio/explosion.wav');
        game.load.audio('bgMusic', 'assets/audio/bgAudio.wav');
    },
    create: function() {
        game.state.start('menu'); 
    }
}