# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description
1. 操作:
    > 1P: 上下左右鍵移動 
        z-普通攻擊 x-飛彈 c-霰彈
    > 2P: WASD移動
        j-普通攻擊 k-飛彈 l-霰彈 
# Basic Components Description : 
1. Jucify mechanisms : 
    > Level: 關卡隨時間增加，並增加敵人數量，在通過Level 5後即可通過遊戲。
    > Skill: 使用者藉由擊中敵人增加Power，可消耗Power使用更為強力的攻擊。
2. Animations : 
    > 玩家和敵機的角色動畫
    > 玩家飛彈的飛行動畫
3. Particle Systems :
    > 擊中敵人爆炸時會以emitter製造爆炸效果
    > 飛彈飛行時尾端以emitter製造電漿推進效果
4. Sound effects :
    > 擊毀敵機時的爆炸音效
    > 選單和遊戲的背景音樂
5. Leaderboard : 
    > 進入遊戲前以prompt填入玩家名稱
    > 以firebase real time database 紀錄遊戲結果，並在遊戲結束時顯示前12名高分玩家紀錄。
6. UI:
    > Volume: 進入遊戲後，可以鍵盤1來減小音量，2來增大音量。
    > Pause: 按下P可暫停遊戲，可藉點及任意處繼續進行遊戲。
# Bonus Functions Description : 
1. 追蹤飛彈 :
    > 玩家可消耗 10 Power 發射追蹤飛彈，追蹤飛彈會根據敵機位置更新自身sprite的rotation，並同時更新emitter的x, y方向
    速度製造出飛彈轉彎的效果，自動瞄準是由得到enemy group first alive位置來判斷轉彎方向，而較複雜的則是emitter起始位
    置需在圖片旋轉後依然對準飛彈尾端才能製造電漿推進的效果。
    ![avatar](assets/README/missileDemo.png)
2. 特殊子彈:
    > 玩家可消耗 1 Power 發射霰彈雷射，子彈會由玩家朝前方進行大範圍攻擊。
    ![avatar](assets/README/spreadLaserDemo.png)
3. 多人遊戲(off-line):
    > 額外增加一名玩家P2，以WASD上下左右，並以JKL攻擊。再擊敗敵機可共同增加Power，而施放Skill時則是消耗各自的Power。
4. 回復藥劑:
    > 玩家可藉由碰觸地圖上的回覆藥劑回復HP。
    ![avatar](assets/powerUp.png)

